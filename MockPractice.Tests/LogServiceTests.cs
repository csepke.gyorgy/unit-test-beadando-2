﻿namespace MockPractice.Tests
{
    using Moq;
    using NUnit.Framework;
    using System.Linq;

    [TestFixture]
    public class LogServiceTests
    {
        private LogService logService;

        [SetUp]
        public void Setup()
        {
            logService = new LogService();
        }

        [Test]
        public void LogService_Log_Should_InvokeLoggerLogs_When_MessageIsProvidedAndLogLevelIsInfo()
        {
            //arrange
            string message = "Info log";
            LogLevel logLevel = LogLevel.Info;

            Mock<ILogger> loggerMock1 = new Mock<ILogger>();
            Mock<ILogger> loggerMock2 = new Mock<ILogger>();
            Mock<ILogger> loggerMock3 = new Mock<ILogger>();
            loggerMock1.Setup(l => l.SupportsLogLevel(LogLevel.Info)).Returns(true);
            loggerMock2.Setup(l => l.SupportsLogLevel(LogLevel.Info)).Returns(true);
            loggerMock3.Setup(l => l.SupportsLogLevel(LogLevel.Info)).Returns(true);
            ILogger logger1 = loggerMock1.Object;
            ILogger logger2 = loggerMock2.Object;
            ILogger logger3 = loggerMock3.Object;
            logService.RegisterLogger(logger1);
            logService.RegisterLogger(logger2);
            logService.RegisterLogger(logger3);

            //act
            logService.Log(message, logLevel);

            //assert
            string logger1LogMessage = $": {message}";
            loggerMock1.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
            loggerMock2.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
            loggerMock3.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
        }

        [Test]
        public void LogService_Log_Should_InvokeLoggerLogs_When_MessageIsProvidedAndLogLevelIsError()
        {
            //arrange
            string message = "Error log";
            LogLevel logLevel = LogLevel.Error;

            Mock<ILogger> loggerMock1 = new Mock<ILogger>();
            Mock<ILogger> loggerMock2 = new Mock<ILogger>();
            Mock<ILogger> loggerMock3 = new Mock<ILogger>();
            loggerMock1.Setup(l => l.SupportsLogLevel(LogLevel.Error)).Returns(false);
            loggerMock2.Setup(l => l.SupportsLogLevel(LogLevel.Error)).Returns(true);
            loggerMock3.Setup(l => l.SupportsLogLevel(LogLevel.Error)).Returns(true);
            ILogger logger1 = loggerMock1.Object;
            ILogger logger2 = loggerMock2.Object;
            ILogger logger3 = loggerMock3.Object;
            logService.RegisterLogger(logger1);
            logService.RegisterLogger(logger2);
            logService.RegisterLogger(logger3);

            //act
            logService.Log(message, logLevel);

            //assert
            string logger1LogMessage = $": {message}";
            loggerMock1.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Never);
            loggerMock2.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
            loggerMock3.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
        }

        [Test]
        public void LogService_Log_Should_InvokeLoggerLogs_When_MessageIsProvidedAndLogLevelIsDebug()
        {
            //arrange
            string message = "Debug log";
            LogLevel logLevel = LogLevel.Debug;

            Mock<ILogger> loggerMock1 = new Mock<ILogger>();
            Mock<ILogger> loggerMock2 = new Mock<ILogger>();
            Mock<ILogger> loggerMock3 = new Mock<ILogger>();
            loggerMock1.Setup(l => l.SupportsLogLevel(LogLevel.Debug)).Returns(false);
            loggerMock2.Setup(l => l.SupportsLogLevel(LogLevel.Debug)).Returns(false);
            loggerMock3.Setup(l => l.SupportsLogLevel(LogLevel.Debug)).Returns(true);
            ILogger logger1 = loggerMock1.Object;
            ILogger logger2 = loggerMock2.Object;
            ILogger logger3 = loggerMock3.Object;
            logService.RegisterLogger(logger1);
            logService.RegisterLogger(logger2);
            logService.RegisterLogger(logger3);

            //act
            logService.Log(message, logLevel);

            //assert
            string logger1LogMessage = $": {message}";
            loggerMock1.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Never);
            loggerMock2.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Never);
            loggerMock3.Verify(l => l.Log(It.Is<string>(str => str.Contains($": {message}")), logLevel), Times.Once);
        }
    }
}
