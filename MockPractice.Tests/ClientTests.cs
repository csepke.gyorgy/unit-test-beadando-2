﻿namespace MockPractice.Tests
{
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ClientTests
    {
        private Client client;
        private Mock<IContentFormatter> contentFormatterMock;
        private Mock<IService> serviceMock;

        [SetUp]
        public void Setup()
        {
            contentFormatterMock = new Mock<IContentFormatter>();
            serviceMock = new Mock<IService>();

            client = new Client(serviceMock.Object, contentFormatterMock.Object);
        }

        [Test]
        public void Client_GetIdentityFormatted_Should_ReturnFormattedIdentity_When_Invoked()
        {
            // arrange
            // act
            string result = client.GetIdentityFormatted();

            // assert
            Assert.That(result.StartsWith("<formatted>"));
            Assert.That(result.EndsWith("</formatted>"));
            Assert.That(result.Contains(client.GetIdentity()));
        }

        [Test]
        public void Client_GetServiceName_Should_ReturnNameOfService_When_Invoked()
        {
            // arrange
            string mockServiceName = "Service name";
            serviceMock.SetupGet(l => l.Name).Returns(mockServiceName);

            // act
            string result = client.GetServiceName();

            // assert
            Assert.That(result == mockServiceName);
        }

        [Test]
        public void Client_GetIdentity_Should_ReturnIdentityAsString_When_Invoked()
        {
            // arrange
            // act
            string result = client.GetIdentity();

            // assert
            Assert.That(result == "2");
        }

        [Test]
        public void Client_Dispose_Should_InvokeDisposeOnce_When_Invoked()
        {
            // arrange
            // act
            client.Dispose();

            // assert
            serviceMock.Verify(l => l.Dispose(), Times.Once);
        }

        [TestCase(0)]
        [TestCase(10)]
        [TestCase(-10)]
        [TestCase(long.MinValue)]
        [TestCase(long.MaxValue)]
        public void Client_GetContentFormatted_Should_ReturnFormattedContent_When_ServiceIsConnected(long id)
        {
            // arrange
            serviceMock.SetupGet(l => l.IsConnected).Returns(true);
            serviceMock.Setup(l => l.GetContent(id)).Returns(id.ToString());
            contentFormatterMock.Setup(l => l.Format(id.ToString())).Returns($"<formatted> {id.ToString()} </formatted>");

            // act
            string result = client.GetContentFormatted(id);

            // assert
            serviceMock.Verify(l => l.Connect(), Times.Never);
            Assert.That(result == $"<formatted> {id.ToString()} </formatted>");
        }

        [TestCase(0)]
        [TestCase(10)]
        [TestCase(-10)]
        [TestCase(long.MinValue)]
        [TestCase(long.MaxValue)]
        public void Client_GetContentFormatted_Should_ReturnFormattedContent_When_ServiceIsNotConnected(long id)
        {
            // arrange
            serviceMock.SetupGet(l => l.IsConnected).Returns(false);
            serviceMock.Setup(l => l.GetContent(id)).Returns(id.ToString());
            contentFormatterMock.Setup(l => l.Format(id.ToString())).Returns($"<formatted> {id.ToString()} </formatted>");

            // act
            string result = client.GetContentFormatted(id);

            // assert
            serviceMock.Verify(l => l.Connect(), Times.Once);
            Assert.That(result == $"<formatted> {id.ToString()} </formatted>");
        }

        [TestCase(0)]
        [TestCase(10)]
        [TestCase(-10)]
        [TestCase(long.MinValue)]
        [TestCase(long.MaxValue)]
        public void Client_GetContent_Should_ReturnContent_When_ServiceIsNotConnected(long id)
        {
            // arrange
            serviceMock.SetupGet(l => l.IsConnected).Returns(false);
            serviceMock.Setup(l => l.GetContent(id)).Returns(id.ToString());
            
            // act
            string result = client.GetContent(id);

            // assert
            serviceMock.Verify(l => l.Connect(), Times.Once);
            Assert.That(result == id.ToString());
        }

        [TestCase(0)]
        [TestCase(10)]
        [TestCase(-10)]
        [TestCase(long.MinValue)]
        [TestCase(long.MaxValue)]
        public void Client_GetContent_Should_ReturnContent_When_ServiceIsConnected(long id)
        {
            // arrange
            serviceMock.SetupGet(l => l.IsConnected).Returns(true);
            serviceMock.Setup(l => l.GetContent(id)).Returns(id.ToString());
            
            // act
            string result = client.GetContent(id);

            // assert
            serviceMock.Verify(l => l.Connect(), Times.Never);
            Assert.That(result == id.ToString());
        }
    }
}