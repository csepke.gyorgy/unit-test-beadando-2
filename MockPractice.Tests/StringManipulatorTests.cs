﻿namespace MockPractice.Tests
{
    using Moq;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class StringManipulatorTests
    {
        private StringManipulator stringManipulator;

        [SetUp]
        public void Setup()
        {
            stringManipulator = new StringManipulator();
        }

        [TestCase("sadas", "aasds")]
        [TestCase("sss", "sss")]
        [TestCase("aaa", "aaa")]
        [TestCase("aaas", "aaas")]
        [TestCase("saaa", "aaas")]
        [TestCase("sasaa", "aaass")]
        [TestCase("sasaas", "aaasss")]
        public void StringManipulator_Transform_Should_ReturnVowelsForwardedString_When_InvokedWithNonNullOrWhiteSpaceString(string s, string expectedResult)
        {
            // arrange
            // act
            string result = stringManipulator.Transform(s);

            // assert
            Assert.That(result == expectedResult);
        }

        [TestCase("")]
        [TestCase("\t\t")]
        [TestCase("     ")]
        [TestCase(null)]
        public void StringManipulator_Transform_Should_ThrowArgumentException_When_InvokedWithNullOrWhiteSpaceString(string s)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentException), () => stringManipulator.Transform(s));
        }
    }
}
